package com.example.sayem43.mvppatternexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.example.sayem43.mvppatternexample.listeners.OnItemClickListener;
import com.example.sayem43.mvppatternexample.models.Result;
import com.example.sayem43.mvppatternexample.presenter.CountryPresenter;
import com.example.sayem43.mvppatternexample.ui.adapters.SampleRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity implements CountryPresenter.CountryPresenterListener {

    @BindView(R.id.rv_data)
    RecyclerView rvData;
    CountryPresenter countryPresenter;
    List<Result> results = new ArrayList<>();
    //private CompositeDisposable compositeDisposable;
    Unbinder unbinder;
    SampleRecyclerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //initialize adapter and on rv Item click listener
        adapter = new SampleRecyclerAdapter(results, new OnItemClickListener() {
            @Override
            public void onItemClick(Result item, int pos) {
                Toast.makeText(MainActivity.this, "Item clicked! Position: " + pos, Toast.LENGTH_SHORT).show();
            }
        });
        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        rvData.setLayoutManager(layoutManager);
        rvData.setItemAnimator(new DefaultItemAnimator());
        rvData.setAdapter(adapter);
        // Listener call
        countryPresenter = new CountryPresenter(this, this);
        countryPresenter.getCountries();
    }

    @Override
    public void countriesReady(List<Result> countries) {

        for (Result country : countries) {
            Log.d("RETROFIT", country.getName() + "\n"
                    + " - Alpha2:  " + country.getAlpha2Code() + " \n"
                    + " - Alpha3: " + country.getAlpha3Code());
            results.add(country);
        }

        adapter.notifyDataSetChanged();

        Log.e("***funCount", "Yes, I'm in!");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unbinder.unbind();
    }
}
