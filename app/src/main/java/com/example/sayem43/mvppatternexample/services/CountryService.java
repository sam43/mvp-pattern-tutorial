package com.example.sayem43.mvppatternexample.services;

import com.example.sayem43.mvppatternexample.utils.Constants;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
  Created by Sayem43 on 11/5/2017.
 */


/**
 *  We can define different api calling methods or the same m
 *  In the same interface off course!
 * */

public class CountryService {
    //private static String BASE_URL = "http://services.groupkt.com/"; //"http://www.json-generator.com/";

    /*public interface  CountryAPI{
        @GET("country/get/all")
        Call<RestyResponse> getResults();
    }*/

    public CountryAPI getAPI(){
        Retrofit retrofit = new Retrofit
                .Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit.create(CountryAPI.class);
    }
}
