package com.example.sayem43.mvppatternexample.singletons;

/**
 * Created by Sayem43 on 11/11/2017.
 */

class MySingleton {
    private static final MySingleton ourInstance = new MySingleton();

    private MySingleton() {
    }

    static MySingleton getInstance() {
        return ourInstance;
    }
}
