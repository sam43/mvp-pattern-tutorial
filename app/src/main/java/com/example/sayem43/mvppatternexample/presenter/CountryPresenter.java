package com.example.sayem43.mvppatternexample.presenter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.sayem43.mvppatternexample.models.RestyResponse;
import com.example.sayem43.mvppatternexample.models.Result;
import com.example.sayem43.mvppatternexample.services.CountryService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Sayem43 on 11/5/2017.
 */

@SuppressWarnings("ALL")
public class CountryPresenter {
    private final Context context;
    private final CountryPresenterListener mListener;
    private final CountryService countryService;
    private ArrayList<Result> resultArrayList;
    //private ArrayList<Result> messageArrayList;

    public CountryPresenter(CountryPresenterListener listener, Context context){
        this.mListener = listener;
        this.context = context;
        this.countryService = new CountryService();
    }

    public void getCountries(){
        countryService
                .getAPI()
                .getResults()
                .enqueue(new Callback<RestyResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<RestyResponse> call, @NonNull Response<RestyResponse> response) {

                        Log.d("** Result","" + response.body() + "" + response.isSuccessful() + "" + response.raw() + "" + response.errorBody());

                        if (response.isSuccessful()) {

                            RestyResponse result = response.body();

                            resultArrayList = new ArrayList<>();

                            if (result != null) {
                                resultArrayList = result.getRestResponse().getResult();

                                for (int i = 0; i < result.getRestResponse().getMessages().size(); i++)
                                    Log.d("** Result","" + result.getRestResponse().getMessages().get(i));
                                if(resultArrayList != null) {
                                    try {
                                        mListener.countriesReady(resultArrayList);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Log.e("***Exception","Error from listener! (catch)");
                                    }
                                } else {
                                    Log.d("** Null", "Result == null");
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<RestyResponse> call, Throwable t) {
                        try {
                            throw  new InterruptedException("Error in server!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    public interface CountryPresenterListener {
        void countriesReady(List<Result> countries);
    }
}