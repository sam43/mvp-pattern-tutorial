package com.example.sayem43.mvppatternexample.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.sayem43.mvppatternexample.R;
import com.example.sayem43.mvppatternexample.listeners.OnItemClickListener;
import com.example.sayem43.mvppatternexample.models.Result;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Sayem43 on 11/5/2017.
 */

public class SampleRecyclerAdapter extends RecyclerView.Adapter<SampleRecyclerAdapter.SampleViewHOlder> {

    private List<Result> resData;
    private OnItemClickListener onItemClickListener;

    public SampleRecyclerAdapter(List<Result> resData, OnItemClickListener onItemClickListener) {
        this.resData = resData;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public SampleViewHOlder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_info, parent, false);
        return new SampleViewHOlder(view);
    }

    @Override
    public void onBindViewHolder(SampleViewHOlder holder, int position) {
        Result result = resData.get(position);
        holder.tvCountry.setText(result.getName());
        holder.tvAlpha2.setText(result.getAlpha2Code());
        holder.tvAlpha3.setText(result.getAlpha3Code());
        holder.bind(result, onItemClickListener, position);
    }

    @Override
    public int getItemCount() {
        return resData.size();
    }

    class SampleViewHOlder extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_country)
        TextView tvCountry;
        @BindView(R.id.tv_alpha2)
        TextView tvAlpha2;
        @BindView(R.id.tv_alpha3)
        TextView tvAlpha3;

        SampleViewHOlder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(final Result item, final OnItemClickListener listener, final int position) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    //position = SampleRecyclerAdapter.this.getItemViewType(position);
                    listener.onItemClick(item, position);
                }
            });
        }
    }
}
