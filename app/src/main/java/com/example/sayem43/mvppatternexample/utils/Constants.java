package com.example.sayem43.mvppatternexample.utils;

/**
 * Created by Sayem43 on 11/5/2017.
 */

public interface Constants {
    String BASE_URL = "http://services.groupkt.com/";
    String COUNTRY_ALL = "country/get/all";
}
