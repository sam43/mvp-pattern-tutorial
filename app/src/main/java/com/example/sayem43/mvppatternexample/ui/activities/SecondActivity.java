package com.example.sayem43.mvppatternexample.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.sayem43.mvppatternexample.R;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
    }
}
