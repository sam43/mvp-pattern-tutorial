package com.example.sayem43.mvppatternexample.listeners;

import com.example.sayem43.mvppatternexample.models.Result;

/**
 * Created by Sayem43 on 11/6/2017.
 */

public interface OnItemClickListener {
    void onItemClick(Result item, int position);

}
