package com.example.sayem43.mvppatternexample.services;

import com.example.sayem43.mvppatternexample.models.RestyResponse;
import com.example.sayem43.mvppatternexample.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;

/*
  Created by Sayem43 on 11/5/2017.
 */

/**
 *  We can use more than one @GET, @POST, @QUERY, @PART, @MULTIPART etc. method to call an api
 *  In the same interface off course!
 * */


public interface CountryAPI {
    @GET(Constants.COUNTRY_ALL)
    Call<RestyResponse> getResults();
}
