
package com.example.sayem43.mvppatternexample.models;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RestResponse {

    @SerializedName("messages")
    @Expose
    private ArrayList<String> messages = null;
    @SerializedName("result")
    @Expose
    private ArrayList<Result> result = null;

    public ArrayList<String> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<String> messages) {
        this.messages = messages;
    }

    public ArrayList<Result> getResult() {
        return result;
    }

    public void setResult(ArrayList<Result> result) {
        this.result = result;
    }

}
